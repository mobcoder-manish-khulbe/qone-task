import logo from "./logo.svg";
import "./App.css";
import Main from "./Components/Navbar";
import { Provider } from "./context";

function App() {
  return (
    <div className="App">
      <Provider>
        <Main />
      </Provider>
    </div>
  );
}

export default App;
