import React from "react";
import "./index.css";
import DeleteItemPopup from "../Delete";
import FixedEdit from "../fixedEditItem";

const CustomModal = ({ state, updatemodalstate, currentname, itemData, modal_type}) => {
  const modelContentRef = React.useRef();

  // handle outside click
  const handleClickOutside = (event) => {
    if (
      modelContentRef.current &&
      !modelContentRef.current.contains(event.target)
    ) {
      updatemodalstate();
    }
  };

  React.useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  if (state)
    return (
      <div className="modal">
        <div ref={modelContentRef} className="model-content">
          {modal_type == 'edit' ? <FixedEdit onClick={updatemodalstate} itemData={itemData} currentname={currentname}/>
           : <DeleteItemPopup onClick={updatemodalstate} itemData={itemData} currentname={currentname}/>}
        </div>
      </div>
    );

  return null;
};
export default CustomModal;