import styled from "styled-components";

const CustomInput = styled.input`
width: 96%;
padding: 0.8rem;
font-size: 1rem;
border-radius: 0.5rem;
margin: 0.5rem 0 1rem;
border: 0.15rem solid #CBCBCB;
`;
export default CustomInput;
