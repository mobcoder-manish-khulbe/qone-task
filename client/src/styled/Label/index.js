import styled from "styled-components";

const LabelTag = styled.label`
color: #808080;
    font-weight: 600;
    text-align: start;
    display: block;
`;

export default LabelTag;
