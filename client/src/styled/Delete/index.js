import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Buttons";
import ActionSection from "../ActionSection";
import EditItem from "../../Components/EditItems";
import { deleteItem } from "../../service/userServices";
import { connect } from "react-redux";
import { userDisplayData } from "../../redux/actions";

const NoKeep=styled.button`
    margin: 20px auto;
    padding: 0.8rem 2.5rem;
    color: darkred;
    border-radius: 0.5rem;
    font-weight: 400;
    border: none;
    font-size: 1rem;
    cursor: pointer;
    width: 15rem;
    background-color: #fff;
    border:1px solid darkred;
`

const DeleteBtn=styled.button`
display: block;
margin: 20px auto;
padding: 0.8rem 3.5rem;
color: #fff;
border-radius: 0.5rem;
font-weight: 600;
border: none;
font-size: 1rem;
cursor: pointer;
width: 15rem;
background-color: darkred;
`

const MessageSection = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: center;
  box-sizing: border-box;
  padding: 0.5rem 1.5rem;
//   gap: 2rem;
//   margin-top: 2rem;
  background-color:white
`;
const Title = styled.div`
  width: 100%;
  background-color: darkred;
  padding: 1rem;
  box-sizing: border-box;
  border-radius: 0.5rem 0.5rem 0 0;
  color: #fff;
  font-weight: 600;
`;

const CustomActionSection = styled(ActionSection)`
  justify-content: space-around;
  gap: 1rem;
  box-sizing: border-box;
`;
const Message = styled.p`
  color: darkred;
  font-weight: 600;
`;

function DeleteItemPopup({onClick, itemData, userDisplayData}) {
   const updateDelete=(itemData,e)=>{
      e.preventDefault()
      let obj = {
        id : itemData._id
      }
      deleteItem(obj)
      
      setTimeout(()=>{
        userDisplayData();
      },2000)
      
      setTimeout(()=>{
        onClick();
      },1000)
   }
  return (
    <>
      <Title>Warning</Title>
      <MessageSection style={{ flex: 1 }}>
        <Message>Are you Sure?</Message>
        <CustomActionSection>
          <NoKeep
          onClick={()=> onClick()}
          >
            No, Keep
          </NoKeep>
          <DeleteBtn  onClick={(e)=>updateDelete(itemData,e)}>
            Yes, Delete
          </DeleteBtn>
        </CustomActionSection>
      </MessageSection>
    </>
  );
}

const mapStateToProps = state => {
  return {
    loading: false,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    userDisplayData: data => {
      dispatch(userDisplayData(data));
    },
   
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DeleteItemPopup)
