import styled from "styled-components";
const Button = styled.button`
display: block;
margin: 20px auto;
padding: 0.8rem 3.5rem;
color: #fff;
border-radius: 0.5rem;
font-weight: 600;
border: none;
font-size: 1rem;
cursor: pointer;
width: 15rem;
background-color: orange;
`;


export default Button;
