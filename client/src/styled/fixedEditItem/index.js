
import React, { useState }  from "react";
import styled from "styled-components";
import CustomInput from "../../styled/CustomInput";
import Label from "../../styled/Label";
import MainSection from "../../styled/Main";
import Button from "../../styled/Buttons";
import ActionSection from "../../styled/ActionSection";
import {EditService} from '../../service/userServices';
import { connect } from "react-redux";
import { userDisplayData } from "../../redux/actions";

const Form = styled.form`
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
`;
const Container = styled.div`
  width: 50rem;
`;
const Title = styled.div`
  width: 100%;
  background-color: ${(props) => props.theme.primaryColor};
  padding: 1rem;
  box-sizing: border-box;
  border-radius: 0.5rem 0.5rem 0 0;
  color: #fff;
  font-weight: 600;
  background-color:orange
`;
const OutlineButton=styled.button`
    margin: 20px auto;
    padding: 0.8rem 2.5rem;
    color: orange;
    border-radius: 0.5rem;
    font-weight: 400;
    border: none;
    font-size: 1rem;
    cursor: pointer;
    width: 15rem;
    background-color: #fff;
    border:1px solid orange;
`
const MainContainer = styled.div`
  width: 50rem;
`;



const Select = styled.select`
  display: "block";
  font-size: 1rem;
  padding: 1rem;
  width: 100%;
  margin: 0.5rem 0;
  border-radius: 0.5rem;
  boxsizing: border-box;
`;

const SelectItem = styled.option`
  width: 100%;
  font-size: 1rem;
`;
const FieldSection = styled.div`
  width: 100%;
`;

const CustomActionSection = styled(ActionSection)`
  justify-content: end;
  gap: 1rem;
`;
const FormMain=styled.div`
background-color: white;
padding:40px 0px
`

function FixedEdit({onClick, itemData, getData, userDisplayData}) {
  const [selectedValue, setSelectedValue] = useState('');
  const [name, setName] = useState('')

  const updateItem = (itemData,e)=>{
      e.preventDefault()
      let obj = {
        name : name ? name : itemData.name,
        state : selectedValue ? selectedValue : itemData.state,
        id : itemData._id
      }
      EditService(obj);
      
      setTimeout(()=>{
        userDisplayData();
      },2000)

      setTimeout(()=>{
        onClick();
      },1000)
  }

  const handleSelectChange = (e) => {
    setSelectedValue(e.target.value);
  };

  const handelCancel = () => {
    onClick();
  }

  return (
    <MainContainer>
    <Title>Edit Item</Title>
    <FormMain>
    <Form >
      <MainSection>
        <FieldSection>
          <div>
            <Label>Enter  Name</Label>
            <CustomInput
              value={name || itemData.name}
              name="userName"
              placeholder="Enter User Name"
              onChange={(e)=> setName(e.target.value)}
            />
          
          </div>
          <div>
            <Label>Select State</Label>
            <Select
              name="state"
              value={selectedValue || itemData.state}
              onChange={handleSelectChange}
            >
              <SelectItem value="" disabled>
                Select A State
              </SelectItem>
              <SelectItem value={"active"}>Active</SelectItem>
              <SelectItem value={"inactive"}>Not Active</SelectItem>
            </Select>

          </div>
        </FieldSection>
      </MainSection>
      <ActionSection>
        <Button type="submit" onClick={(e)=>updateItem(itemData,e)} >Save</Button>
        <OutlineButton
          onClick={() => handelCancel()}
          >
            Cancel
          </OutlineButton>
      </ActionSection>
    </Form>
    </FormMain>
    </MainContainer>
       
  );
}

const mapStateToProps = state => {
  return {
    loading: false,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    userDisplayData: data => {
      dispatch(userDisplayData(data));
    },
   
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FixedEdit)

