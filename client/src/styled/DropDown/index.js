import styled from "styled-components";
 export const Select = styled.select`
  display: "block";
  font-size: 1rem;
  padding: 1rem;
  width: 100%;
  margin: 0.5rem 0;
  border-radius: 0.5rem;
  boxsizing: border-box;
`;

  export  const SelectItem = styled.option`
  width: 100%;
  font-size: 1rem;
`;

