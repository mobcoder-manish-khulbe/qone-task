import { addItemService, userDisplayService, userDisplayServices, userFetchServices, userLoginServices } from "../../service/userServices"

export const LOGIN =  "LOGIN"
export const LOGIN_REQUEST =  "LOGIN_REQUEST"
export const LOGIN_ERROR =  "LOGIN_ERROR"
export const ADD_ITEM = "ADD_ITEM"
export const ADD_ITEM_REQUEST = "ADD_ITEM_REQUEST"
export const ADD_ITEM_ERROR = "ADD_ITEM_ERROR"
export const GET_DATA = "GET_DATA"
export const GET_REQUEST = "GET_REQUEST";
export const GET_ERROR = "GET_ERROR";
export const DISPLAY_DATA = "GET_DATA";
export const DISPLAY_REQUEST = "GET_REQUEST";
export const DISPLAY_ERROR = "GET_ERROR";
export function loginRequest(){
    return{
        type: LOGIN_REQUEST,
       
    }
}


export function loginSuccess(data){
    return{
        type: LOGIN,
        payload : data
    }
}

export function loginError(data){
    return{
        type: LOGIN_ERROR,
        payload : data
    }
}


export function addItemRequest(){
    return{
        type: ADD_ITEM_REQUEST,
       
    }
}


export function addItemSuccess(data){
    return{
        type: ADD_ITEM,
        payload : data
    }
}

export function addItenError(data){
    return{
        type: ADD_ITEM_ERROR,
        payload : data
    }
}


export function GetDataRequest() {
    return {
      type: GET_REQUEST,
    };
  }
  
  export function GetDataSuccess(data) {
    return {
      type: GET_DATA,
      payload: data,
    };
  }
  
  export function GetDataError(data) {
    return {
      type: GET_ERROR,
      payload: data,
    };
  }
  export function DisplayRequest() {
    return {
      type: DISPLAY_REQUEST,
    };
  }
  
  export function DisplayRequestSuccess(data) {
    return {
      type: DISPLAY_DATA,
      payload: data,
    };
  }
  
  export function DisplayRequestError(data) {
    return {
      type: DISPLAY_ERROR,
      payload: data,
    };
  }
const userLogin=(data)=>{
    return dispatch=>{
        dispatch(loginRequest())
        userLoginServices(data).then(res =>{
            if(res.status==200){
                dispatch(loginSuccess(res.data))
                localStorage.setItem("userId", res?.data?.responseData?.userId)
                localStorage.setItem("token",res?.data?.responseData?.accessToken)
            }else{
                dispatch(loginSuccess(res.error))
            }
        }).catch(error=>{
            dispatch(loginError(error))
        })
        
    }
}

const addItem = (data) => {
  return async (dispatch) => {
    try {
      dispatch(addItemRequest());
      const res = await addItemService(data);
      
      if (res.data.statusCode === 200) {
        dispatch(addItemSuccess(res.data));
        localStorage.setItem("insertId", res?.data?.responseData?.insertedId);
      } else {
        dispatch(addItemSuccess(res.error));
      }
    } catch (error) {
      dispatch(addItenError(error));
    }
  };
};



const userFetchData = (data) => {
  return (dispatch) => {
    dispatch(GetDataRequest());

    userFetchServices(data)
      .then((res) => {
        if (res.statusCode === 200) {
          dispatch(GetDataSuccess(res.responseData));
        } else {
          // dispatch(GetDataSuccess(res.error));
        }
      })
      .catch((error) => {
        dispatch(GetDataError(error));
      });
  };
};
  const userDisplayData = () => {
    return (dispatch) => {
      dispatch(DisplayRequest());
      userDisplayService()
        .then((res) => {
          if (res.statusCode === 200) {
            dispatch(DisplayRequestSuccess(res.responseData));
          } else {
            // dispatch(DisplayRequestSuccess(res.error));
          }
        })
        .catch((error) => {
          dispatch(DisplayRequestError(error));
        });
    };
  };

  const userUpdateData = (data) => {
    return (dispatch) => {
      dispatch(DisplayRequest());
      userDisplayService()
        .then((res) => {
          if (res.statusCode === 200) {
            dispatch(DisplayRequestSuccess(res.responseData));
          } else {
            // dispatch(DisplayRequestSuccess(res.error));
          }
        })
        .catch((error) => {
          dispatch(DisplayRequestError(error));
        });
    };
  };

  
export { userLogin, addItem, userFetchData , userDisplayData};