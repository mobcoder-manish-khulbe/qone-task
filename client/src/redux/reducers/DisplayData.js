import { DISPLAY_REQUEST, DISPLAY_DATA } from "../actions";

const intialState = {
  isLoading: false,
  FetchDataDetails: [],
};

function DisplayDataReducer(state = intialState, action) {
 
  switch (action.type) {
    case DISPLAY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case DISPLAY_DATA:
      return {
        ...state,
        FetchDataDetails: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}

export default DisplayDataReducer ;