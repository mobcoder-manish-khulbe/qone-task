import { GET_DATA, GET_REQUEST } from "../actions";

const intialState = {
  isLoading: false,
  FetchDataDetails: [],
};

function fetchDataReducer(state = intialState, action) {
 

  switch (action.type) {
    case GET_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case GET_DATA:
      return {
        ...state,
        FetchDataDetails: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}

export default fetchDataReducer;