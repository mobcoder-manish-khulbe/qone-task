import { LOGIN ,LOGIN_REQUEST} from "../actions";

const intialState = {
    isLoading:false,
    userDetails:[]
};

function loginReducer(state = intialState, action){
    
    switch(action.type){
        case LOGIN_REQUEST:
            return{
                ...state,
                isLoading:true
            };
        case LOGIN:
            return{
                ...state,
                userDetails:action.payload,
                isLoading:false
            };
            default:
                return state;
    }
}

export default loginReducer;