import { combineReducers } from "redux";
import addItemReducer from "./addItemReducer";
import fetchDataReducer from "./fetchData";
import loginReducer from "./login";
import DisplayDataReducer from "./DisplayData";

const rootReducer  =  combineReducers({

    userLogin : loginReducer,
    addItem : addItemReducer,
    fetchData: fetchDataReducer,
    displayData:DisplayDataReducer,
})

export default rootReducer;