import { ADD_ITEM ,ADD_ITEM_REQUEST} from "../actions";

const intialState = {
    isLoading:false,
    items:[]
};

function addItemReducer(state = intialState, action){
    
    switch(action.type){
        case ADD_ITEM_REQUEST:
            return{
                ...state,
                isLoading:true
            };
        case ADD_ITEM:
            return{
                ...state,
                userDetails:action.payload,
                isLoading:false
            };
            default:
                return state;
    }
}

export default addItemReducer;