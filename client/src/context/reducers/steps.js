export function step(state, action) {
    
    switch (action.type) {
      case "STEP_DATA":
        return { ...state, step: action.payload };
      default:
        return state;
    }
  }