import rootReducer from "../reducer";
import thunk from "redux-thunk";
import { applyMiddleware, configureStore } from "@reduxjs/toolkit";

                                                                                                         

const store = configureStore({
    reducer:rootReducer,
    // devTools: process.env.NODE_ENV !== 'production',
    //devTools: process.env.NEXTAUTH_URL!=='dev',
    middleware: [thunk]
});
export default store;