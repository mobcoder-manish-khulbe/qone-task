import React, { useContext, useState  , useEffect} from "react";
import styled from "styled-components";
import CustomInput from "../../styled/CustomInput";
import Label from "../../styled/Label";
import MainSection from "../../styled/Main";
import Button from "../../styled/Buttons";
import ActionSection from "../../styled/ActionSection";
import { Context } from "../../context";
import CustomModal from "../../styled/Modal";
import { useSelector } from "react-redux";
import { userDisplayData } from "../../redux/actions";
import { connect } from "react-redux";


const Container = styled.div`
  width: 100%;
  background-color:white
`;
const CustomMainSection = styled(MainSection)`
  padding: 0;
`;
const EditBtn = styled.a`
  color: blue;
`;
const DeleteBtn = styled.a`
  color: red;
`;
const CustomActionSection = styled(ActionSection)`
  justify-content: center;
  gap: 2rem;
`;
const Table = styled.table`
  width: 100%;
  border: 0.2rem solid  orange;
  border-collapse: collapse;
`;
const Th1 = styled.th`
  border: 0.2rem solid orange;
  border-collapse: collapse;
  padding: 1rem;
  color: #808080;
  text-align: start;
  width: 24.6%;
`;
const Th2 = styled.th`
  border: 0.2rem solid orange;
  border-collapse: collapse;
  padding: 1rem;
  color: #808080;
  text-align: start;
  width: 13%;
`;
const Td = styled.td`
  border: 0.2rem solid orange;
  border-collapse: collapse;
  padding: 0.5rem 1rem;
  color: #808080;
  text-align:start
`;


const EditButton=styled.button`
    margin: 20px auto;
    padding: 0.8rem 3.5rem;
    color: #ff3030;
    border-radius: 0.5rem;
    font-weight: 400;
    border: none;
    font-size: 1rem;
    cursor: pointer;
    width: 15rem;
    background-color: #fff;
    border:1px solid #ff3030;
`
function EditItem({userDisplayData}) {
  const {state, dispatch} = useContext(Context)
  const [step, setStep] = useState(0)  
  const[state1 , setState1]= useState(false)
  const[state2 , setState2]= useState(false)

  const {FetchDataDetails} = useSelector((state)=>state.displayData)
  const itemsList = FetchDataDetails.itemsList

  const [itemData, setItemData] = useState({})

  const togglemodal_state = () => {
    setState1(true);
    if (state1 == true) {
      setState1(false);
    }
  };

  const togglemodal2_state = () => {
    setState2(true);
    if (state2 == true) {
      setState2(false);
    }
  };
  useEffect(()=>{
    getData()
  },[])
  
  const updateverify = () => {
    setState1(true);
  };
  const deleteverify = () => {
    setState2(true);
  };

  const handleStepChange = (val)=>{
    setStep(val)
    dispatch({
      type: "STEP_DATA",
      payload: val
    })
  }

  const handleLogout = (val) => {
    localStorage.clear();
    setStep(val)
    dispatch({
      type: "STEP_DATA",
      payload: val
    })
  }

  const getData = () => {
    userDisplayData()
  }
  return (
    <>
    <Container>
      <CustomMainSection>
          <Table>
            <thead>
              <tr>
                <Th1>Item Name</Th1>
                <Th1>User Added</Th1>
                <Th1>Date Added</Th1>
                <Th2>State</Th2>
                <Th2>Action</Th2>
              </tr>
            </thead>
            <tbody>
            {itemsList && itemsList.length>0 && itemsList.map((data, index) => {
                return (
                  <tr key={index}>
                    <Td>{data?.itemAddedByUsername}</Td>
                    <Td>{data?.name}</Td>
                    <Td>{data?.createdAt.slice(0,10).split("-").reverse().join("-")}</Td>
                    <Td>{data?.state}</Td>
                    <Td>
                      
                      <div style={{ display: "flex", gap: "10%" }}>
                        <EditBtn  onClick={()=>{updateverify(); setItemData(data);}}   href="#">
                          Edit
                        </EditBtn>
                        <DeleteBtn  onClick={()=>{deleteverify(); setItemData(data);}} href="#">
                          Delete
                        </DeleteBtn>
                      </div>
                    </Td>
                  </tr>
                );
              })}

             
            </tbody>
            
          </Table>
          </CustomMainSection>
    
          <ActionSection>
          <EditButton type="submit" onClick={()=>handleStepChange(1)}>Add more Items</EditButton>
          <Button type="submit" onClick={()=>handleLogout(0)}>LogOut</Button>
        </ActionSection>
      </Container>
      <CustomModal state={state1} itemData={itemData}  updatemodalstate={togglemodal_state} modal_type={'edit'} />
      <CustomModal state={state2} itemData={itemData}  updatemodalstate={togglemodal2_state} modal_type={'del'} />
     
    </>

  );
}
const mapStateToProps = state => {
  return {
    loading: false,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    userDisplayData: data => {
      dispatch(userDisplayData(data));
    },
   
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(EditItem)
