import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import Login from "../Login";
import AddItem from "../AddItem";
import PreviewItem from "../PreviewItem";
import EditItem from "../EditItems";
import { Context } from "../../context";

export default function Main() {
  const { state, dispatch } = useContext(Context);
  const [step, setStep] = useState(0);

  useEffect(() => {
    setStep(state?.step);
  }, [state?.step]);
  const Main = styled.div`
    padding: 10px;
    background-color: lightgray;
  `;
  const NavBar = styled.div`
    display: flex;
    border-left-color: #e8e8e8;
  `;
  const NavItem = styled.div`
    &.active {
      width: calc(100% / 4);
      border: 1px solid orange;
      background-color: orange;
      color: white;
      padding: 10px;
      font-size: 15px;
    }
    width: calc(100% / 4);
    border: 1px solid orange;
    background-color: white;
    color: #ff3030;
    padding: 10px;
    font-size: 15px;
   
  `;
  const Title = styled.div``;

  return (
    <Main>
      <NavBar>
        <NavItem className={step >= 0? "active" : ""}>
          <h2>Step 1</h2>
          <Title>Log In User</Title>
        </NavItem>
        <NavItem className={step >= 1 ? "active" : ""}>
          <h2>Step 2</h2>
          <Title>Add Item</Title>
        </NavItem>
        <NavItem className={step >= 2 ? "active" : ""}>
          <h2>Step 3</h2>
          <Title>Preview Item</Title>
        </NavItem>
        <NavItem className={step >= 3 ? "active" : ""}>
          <h2>Step 4</h2>
          <Title>Edit Item</Title>
        </NavItem>
      </NavBar>
      {step == 0 &&<Login />}
      {step == 1 &&<AddItem />}
      {step == 2 &&<PreviewItem />}
      {step == 3 &&<EditItem />}

    </Main>
  );
}
