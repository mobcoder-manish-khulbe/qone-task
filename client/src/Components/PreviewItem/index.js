import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import CustomInput from "../../styled/CustomInput";
import Label from "../../styled/Label";
import MainSection from "../../styled/Main";
import Button from "../../styled/Buttons";
import ActionSection from "../../styled/ActionSection";
import { Context } from "../../context";
import { userFetchData } from "../../redux/actions";
import { connect, useSelector } from "react-redux";




export const Select = styled.select`
  display: "block";
  font-size: 1rem;
  padding: 1rem;
  width: 100%;
  margin: 0.5rem 0;
  border-radius: 0.5rem;
  boxsizing: border-box;
`;

export const SelectItem = styled.option`
  width: 100%;
  font-size: 1rem;
`;
const FormMain = styled.div`
  background-color: white;
  padding: 40px 0px;
`;
const Form = styled.form`
  width: 70%;
  margin: auto;
`;
const FieldSection = styled.div`
  width: 100%;
`;
const Value = styled.div`
  font-size: 1.2rem;
  padding: 1rem 0;
  color: #808080;
`;
const LabelInner = styled.div`
  display: flex;
  align-items:center
`;

const LabelMain = styled.div`
  width: 50%;
  margin: auto;
`;
const Labels =styled.div`
width:70%;
text-align:start
`
const AddButton=styled.button`
    margin: 20px auto;
    padding: 0.8rem 3.5rem;
    color: orange;
    border-radius: 0.5rem;
    font-weight: 400;
    border: none;
    font-size: 1rem;
    cursor: pointer;
    width: 15rem;
    background-color: #fff;
    border:1px solid orange;
`

 function PreviewItem({userFetchData}) {
  const {state, dispatch} = useContext(Context)
   const [step, setStep] = useState(0)  
   const [itemData, setItemData] = useState(null)

   
   const data = useSelector((state) => state);
   useEffect(() => {
     
     setItemData(data?.fetchData?.FetchDataDetails?.itemsList);
   },[data])
  


  const handleStepChange = (val)=>{
    setStep(val)
    dispatch({
      type: "STEP_DATA",
      payload: val
    })
  }
   
  var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = mm + '/' + dd + '/' + yyyy;

  useEffect(()=>{
    console.log(localStorage.getItem('insertId'),'insertidinsertid')
    userFetchData()
  },[])

  return (
    <FormMain>
      <Form>
        <LabelMain>
          <LabelInner>
            <Labels>Item Name</Labels>
            <Value>{itemData?.name}</Value>
          </LabelInner>
          <LabelInner>
            <Labels>User Added</Labels>
            <Value>{itemData?.itemAddedByUsername} </Value>
          </LabelInner>
          <LabelInner>
            <Labels>Date Added</Labels>
            <Value>{today}</Value>
          </LabelInner>
          <LabelInner>
            <Labels>State</Labels>
            <Value>{itemData?.state}</Value>
          </LabelInner>
        </LabelMain>

        <ActionSection>
          <AddButton type="submit" onClick={() => handleStepChange(1)}>
            Add more Items
          </AddButton>
          <Button type="submit" onClick={() => handleStepChange(3)}>
            Submiit
          </Button>
        </ActionSection>
      </Form>
    </FormMain>
  );
}

const mapStateToProps = state => {
  return state
};
const mapDispatchToProps = dispatch => {
  return {
    userFetchData: data => {
      dispatch(userFetchData(data));
    },
   
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PreviewItem)