import React, { useContext, useState } from "react";
import styled from "styled-components";
import CustomInput from "../../styled/CustomInput";
import Label from "../../styled/Label";
import MainSection from "../../styled/Main";
import Button from "../../styled/Buttons";
import ActionSection from "../../styled/ActionSection";
import { Context } from "../../context";
import { addItem } from "../../redux/actions";
import { connect } from "react-redux";

export const Select = styled.select`
  display: "block";
  font-size: 1rem;
  padding: 1rem;
  width: 100%;
  margin: 0.5rem 0;
  border-radius: 0.5rem;
  boxsizing: border-box;
`;

export const SelectItem = styled.option`
  width: 100%;
  font-size: 1rem;
`;
const FormMain = styled.div`
  background-color: white;
  padding: 40px 0px;
`;
const Form = styled.form`
  width: 50%;
  margin: auto;
`;
const FieldSection = styled.div`
  width: 100%;
`;

function AddItem({ addItem }) {
  const [name, setName] = useState(null);
  const [states, setStates] = useState("active");
  const { state, dispatch } = useContext(Context);
  const [step, setStep] = useState(0);

  const handleStepChange = (val,e) => 
  {
    e.preventDefault();
    setStep(val);

    let obj = {
      state: selectState,
      name: name,
      userId: localStorage.getItem("userId"),
    };

    addItem(obj)

    name && selectState && setTimeout(()=>{
      dispatch({
        type: "STEP_DATA",
        payload: val,
      });
    },2000)
  };

  const [selectState, setSelectState] = useState("");

  const handleSelectChange = (event) => {
    setSelectState(event.target.value); // Update the selected value
  };

  return (
    <FormMain>
      <Form>
        <MainSection>
          <FieldSection>
            <div>
              <Label>Enter Name</Label>
              <CustomInput
                value={name}
                name="userName"
                placeholder="Enter User Name"
                onChange={(e) => setName(e.target.value)}
              />
            </div>
            <div>
              <Label>Select State</Label>
              <Select
                name="state"
                value={selectState}
                onChange={handleSelectChange}
              >
                <SelectItem value="">
                  Select A State
                </SelectItem>
                <SelectItem value={"active"}>Active</SelectItem>
                <SelectItem value={"inactive"}>Not Active</SelectItem>
              </Select>
            </div>
          </FieldSection>
        </MainSection>
        <ActionSection>
          <Button type="submit" onClick={(e) => handleStepChange(2,e)}>
            Add Item
          </Button>
        </ActionSection>
      </Form>
    </FormMain>
  );
}

const mapStateToProps = (state) => {
  return {
    loading: state.addItem.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addItem: (data) => {
      dispatch(addItem(data));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddItem);