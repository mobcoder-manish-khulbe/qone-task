import React, { useContext, useEffect, useState, useTransition } from "react";
import styled from "styled-components";
import CustomInput from "../../styled/CustomInput";
import Label from "../../styled/Label";
import MainSection from "../../styled/Main";
import Button from "../../styled/Buttons";
import ActionSection from "../../styled/ActionSection";
import { Context } from "../../context";
import { userLogin } from "../../redux/actions";
import { connect } from "react-redux";

const FormMain=styled.div`
background-color: white;
padding:40px 0px
`
const Form = styled.form`
  width: 50%;
  margin:auto;
`;
const FieldSection = styled.div`
  width: 100%;
`;

 function Login({userLogin}) {
  const {state, dispatch} = useContext(Context)
  const [step, setStep] = useState(0)  
  const [userName, setUserName] = useState(null);
  const [password, setPassword] = useState(null);


  const handleStepChange = (val)=>{
    setStep(val)
    let obj = {
      username: userName,
      password: password
    }
    if(userName && password){
      userLogin(obj)
      dispatch({
        type: "STEP_DATA",
        payload: val
      })
    }else{
      alert("wrong username or password!!")
    }
  }

  // useEffect(()=>{
  //   userLogin()
  // },[])
  return (
    <FormMain>
    <Form >
      <MainSection>
        <FieldSection>
          <div>
            <Label>Enter User Name</Label>
            <CustomInput
              value={userName}
              name="userName"
              placeholder="Enter User Name"
              onChange={(e)=>setUserName(e.target.value)}
            />
          
          </div>
          <div>
            <Label>Enter Password</Label>
            <CustomInput
              type="password"
              value={password}
              name="password"
              placeholder="Enter Password"
              onChange={(e)=>setPassword(e.target.value)}
            />
           
          </div>
        </FieldSection>
      </MainSection>
      <ActionSection>
        <Button type="submit" onClick={()=>handleStepChange(1)}>Log In</Button>
      </ActionSection>
    </Form>
    </FormMain>
  );
  }
  const mapStateToProps = state => {
    return {
      loading: state.userLogin.isLoading,
    };
  };
  const mapDispatchToProps = dispatch => {
    return {
      userLogin: data => {
        dispatch(userLogin(data));
      },
     
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Login)