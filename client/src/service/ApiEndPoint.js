const auth="sdasdasdasdasd"
const BASE_URL="http://localhost:3001/api/v1/"
export default (() => {

  return {
    AUTH: auth,
    LOGIN_USER: BASE_URL + 'login/userLogin',
    ADD_ITEM: BASE_URL + 'items/addItem',
    FETCH_DATA: BASE_URL + `items/getItemById?itemId=`,
    DISPLAY_DATA:BASE_URL + 'items/getItem',
    EDIT_DATA:BASE_URL+'items/editItem',
    DELETE_DATA:BASE_URL+'items/deleteItem',

  }})();