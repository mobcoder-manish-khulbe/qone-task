import axios from 'axios';
import ApiEndPoint from './ApiEndPoint';
const token = localStorage.getItem("token")

export const userLoginServices = async (data ) => {
   return axios.post(ApiEndPoint.LOGIN_USER, data);
}

export const addItemService = async (data ) => {
   return await axios.post(ApiEndPoint.ADD_ITEM, data,  { headers: { 'Authorization': `Bearer ${token}` } });
}

export async function userFetchServices(payload) {
  let data = payload;
  let url = ApiEndPoint.FETCH_DATA;
  const insertId=localStorage.getItem('insertId')
  const response = await fetch(url+`${insertId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  });
  return response.json();
}

export async function userDisplayService() {
  let url = ApiEndPoint.DISPLAY_DATA;
  const response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
  });
  return response.json();
}

export async function DeleteService(payload) {
  let data = payload
  let url = ApiEndPoint.DELETE_DATA;
  const response = await fetch(url, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  });
  return response.json();
}

export async function EditService(payload) {
  let data = payload
  let url = ApiEndPoint.EDIT_DATA;
  const response = await fetch(`${url}?itemId=${payload.id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
    body: JSON.stringify(payload),
  });
  return response.json();
}

export async function deleteItem(payload) {
  let data = payload
  let url = ApiEndPoint.DELETE_DATA;
  const response = await fetch(`${url}?itemId=${payload.id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${token}`,
    },
  });
  return response;
}








 

 